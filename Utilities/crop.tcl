package require vtk

# Configure bounding box
set xmin -20
set xmax 20
set ymin 80
set ymax 130
set zmin -50
set zmax -25

# Set up input files
set files [list meshes/183EditedTongueMesh00001 meshes/183EditedTongueMesh00005 meshes/183EditedTongueMesh00007]


# Set up the clipping function
vtkBox clipFunction
    clipFunction SetBounds $xmin $xmax $ymin $ymax $zmin $zmax

# Processing pipeline: read file, clip, write

vtkPolyDataReader R

vtkClipPolyData C
    C SetInputConnection [R GetOutputPort]
    C SetClipFunction clipFunction
    C InsideOutOn

vtkPolyDataWriter W
    W SetInputConnection [C GetOutputPort]
    
foreach fn $files {
    puts $fn

    R SetFileName "$fn.vtk"
    W SetFileName "$fn.crop.vtk"

    W Update
}
