## Script for building
# Source & build folders follow Gitlab-CI convention for Docker runner

SRC_DIR=/builds/FullMonte/MeshTool
BUILD_DIR=/builds/FullMonte/MeshTool/Build/Release
INSTALL_DIR=/usr/local/MeshTool

mkdir -p $BUILD_DIR \
 && cd $BUILD_DIR \
 && cmake -G Ninja \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
        -DCGAL_DIR=/build/cgal-5.6.1 \
        -DVTK_DIR=/usr/local/VTK-7.1.1/lib/cmake/vtk-7.1 \
        -DBOOST_ROOT=/build/boost_1_83_0 \
        -DGMP_LIBRARIES=/usr/lib/x86_64-linux-gnu/libgmpxx.so \
        $SRC_DIR \
 && ninja -j8 > build.log 2>&1 \
 && ninja install \
 && rm -rf /builds/FullMonte/MeshTool/Install \
 &&  mv /usr/local/MeshTool /builds/FullMonte/MeshTool/Install \
 &&  cp /builds/FullMonte/MeshTool/Dockerfile /builds/FullMonte/MeshTool/Install
