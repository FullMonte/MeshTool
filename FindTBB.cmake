cmake_minimum_required(VERSION 3.1)

# Find the TBB libraries and headers
find_path(TBB_INCLUDE_DIR NAMES tbb/tbb.h PATHS /usr/include/tbb)
find_library(TBB_LIBRARY NAMES tbb)
find_library(TBBMALLOC_PROXY_LIBRARY NAMES tbbmalloc_proxy PATHS /usr/lib/x86_64-linux-gnu REQUIRED)
find_library(TBB_MALLOC_LIBRARY NAMES tbbmalloc PATHS /usr/lib/x86_64-linux-gnu REQUIRED)

message(STATUS "TBB_MALLOC_LIBRARY found: ${TBB_MALLOC_LIBRARY}")

include(FindPackageHandleStandardArgs)
# Ensure all libraries are found
find_package_handle_standard_args(TBB DEFAULT_MSG TBB_LIBRARY TBB_INCLUDE_DIR TBB_MALLOC_LIBRARY TBBMALLOC_PROXY_LIBRARY)

mark_as_advanced(TBB_INCLUDE_DIR TBB_LIBRARY TBB_MALLOC_LIBRARY TBBMALLOC_PROXY_LIBRARY)

# Set the found variables and the include and library directories
if(TBB_FOUND AND NOT TARGET TBB::tbb)
  add_library(TBB::tbb UNKNOWN IMPORTED)
  set_target_properties(TBB::tbb PROPERTIES
                        IMPORTED_LOCATION "${TBB_LIBRARY}"
                        INTERFACE_INCLUDE_DIRECTORIES "${TBB_INCLUDE_DIR}")
endif()

# if(TBB_MALLOC_FOUND AND NOT TARGET TBB::tbbmalloc)
  add_library(TBB::tbbmalloc UNKNOWN IMPORTED)
  set_target_properties(TBB::tbbmalloc PROPERTIES
                        IMPORTED_LOCATION "${TBB_MALLOC_LIBRARY}"
                        INTERFACE_INCLUDE_DIRECTORIES "${TBB_INCLUDE_DIR}")
# endif()

if(TBBMALLOC_PROXY_FOUND AND NOT TARGET TBB::tbbmalloc_proxy)
  add_library(TBB::tbbmalloc_proxy UNKNOWN IMPORTED)
  set_target_properties(TBB::tbbmalloc_proxy PROPERTIES
                        IMPORTED_LOCATION "${TBBMALLOC_PROXY_LIBRARY}"
                        INTERFACE_INCLUDE_DIRECTORIES "${TBB_INCLUDE_DIR}")
endif()
