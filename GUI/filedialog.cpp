#include "filedialog.h"

#include <QDataStream>
#include <QFileDialog>
#include <QTextStream>
#include <iostream>

#include "ui_filedialog.h"

QStringList FileDialog::filePaths;

FileDialog::FileDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::FileDialog) {
  ui->setupUi(this);

  filePaths.clear();
}

FileDialog::~FileDialog() { delete ui; }

void FileDialog::on_buttonBox_accepted() {
  QString s = ui->textEdit->toPlainText();
  filePaths = s.split("\n", Qt::SkipEmptyParts);
  //    QTextStream ss(stdout);
  //    for(int i = 0 ; i < filePaths.size(); i++){
  //        ss<< filePaths[i]<< "\n";
  //    }
}

void FileDialog::on_buttonBox_rejected() { filePaths.clear(); }

std::vector<std::string> FileDialog::GetInputFile(QWidget* parent) {
  FileDialog dialog(parent);
  dialog.exec();

  std::vector<std::string> paths;
  for (int i = 0; i < filePaths.size(); i++)
    paths.push_back(filePaths.at(i).toStdString());

  return paths;
}

void FileDialog::on_addButton_clicked() {
  QStringList fileNames = QFileDialog::getOpenFileNames(this);

  for (int i = 0; i < fileNames.size(); i++)
    ui->textEdit->append(fileNames.at(i));
}
