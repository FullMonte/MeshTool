#ifndef OFFSETTABLE_H
#define OFFSETTABLE_H

#include <QFocusEvent>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QWidget>
#include <array>
#include <map>
#include <string>
class OffsetTable : public QTableWidget {
  Q_OBJECT

 public:
  explicit OffsetTable(QWidget *parent = 0);
  ~OffsetTable();

  std::map<std::string, std::array<float, 3>> GetRegionOffset() const;
  std::map<int, std::string> GetRegionOrder() const;
  std::map<int, float> GetCellSize() const;
  std::map<int, float> GetFacetSize() const;
  std::map<int, float> GetFacetDis() const;

  void write_variable_columns(float facet_distance, float facet_size,
                              float cell_size);

  void initializeRegions(std::vector<std::string>);

 private:
  virtual void mousePressEvent(QMouseEvent *event);

  std::map<std::string, std::array<float, 3>> regionOffset;

  std::map<int, std::string> regionOrder;

  std::map<int, float> cellSize;

  std::map<int, float> facetDis;

  std::map<int, float> facetSize;

  void updateOffset();

  void updateOrder();

  void updateCellSize();

  void updateFacetDis();

  void updateFacetSize();

 private Q_SLOTS:
  void on_cell_changed(int, int);
};

#endif  // OFFSETTABLE_H
