#include "CompareMeshDialog.hpp"
#include "ui_CompareMeshDialog.h"

#include <QFileDialog>

using namespace std;

string CompareMeshDialog::refPath;
string CompareMeshDialog::testPath;


CompareMeshDialog::CompareMeshDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CompareMeshDialog)
{
    ui->setupUi(this);
    refPath.clear();
    testPath.clear();
}

CompareMeshDialog::~CompareMeshDialog()
{
    delete ui;
}


void CompareMeshDialog::on_buttonBox_accepted()
{
    refPath = ui->refPath_lineEdit->text().toStdString();
    testPath = ui->testPath_lineEdit->text().toStdString();
}

void CompareMeshDialog::on_addTest_button_released()
{
    QString path = QFileDialog::getOpenFileName(this,tr("test mesh path"),QString(),tr("VTK Files(*.vtk)"));
    ui->testPath_lineEdit->setText(path);
}

void CompareMeshDialog::on_addRef_button_released()
{
    QString path = QFileDialog::getOpenFileName(this,tr("reference mesh path"),QString(),tr("VTK Files(*.vtk)"));
    ui->refPath_lineEdit->setText(path);
}


pair<string,string> CompareMeshDialog::GetFiles(QWidget* parent){
    CompareMeshDialog dialog(parent);
    dialog.exec();

    return pair<string,string>(testPath,refPath);
}
