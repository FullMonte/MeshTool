#include "offsettable.h"

#include <QAction>
#include <QHeaderView>
#include <QMenu>
#include <QString>
#include <iostream>
#include <unordered_map>

using namespace std;

#define ORDER_COL 0
#define NAME_COL 1
#define CELL_SIZE_COL 2
#define FACET_SIZE_COL 3
#define FACET_DIS_COL 4
#define OFFSET_X_COL 5
#define OFFSET_Y_COL 6
#define OFFSET_Z_COL 7

OffsetTable::OffsetTable(QWidget* parent) : QTableWidget(parent) {
  connect(this, SIGNAL(cellChanged(int, int)), this,
          SLOT(on_cell_changed(int, int)));
  horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

OffsetTable::~OffsetTable() {}

void OffsetTable::mousePressEvent(QMouseEvent* event) {
  QModelIndex item = indexAt(event->pos());
  bool selected = selectionModel()->isSelected(item);
  QTableWidget::mousePressEvent(event);
  if ((item.row() == -1 && item.column() == -1) || selected) {
    clearSelection();
    const QModelIndex index;
    selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select);
  }
}

void OffsetTable::updateOffset() {
  regionOffset.clear();

  for (int row = 0; row < rowCount(); row++) {
    QTableWidgetItem* itemName = item(row, NAME_COL);
    if (itemName == nullptr || itemName->text().isEmpty()) break;

    string name = itemName->text().toStdString();
    array<float, 3> offsets;

    bool isEmpty = false;
    for (int i = OFFSET_X_COL; i < OFFSET_X_COL + 3; i++) {
      QTableWidgetItem* itemOffset = item(row, i);
      if (itemOffset == nullptr || itemOffset->text().isEmpty()) {
        isEmpty = true;
        break;
      }
      offsets[i - OFFSET_X_COL] = itemOffset->text().toFloat();
    }
    if (isEmpty) break;

    regionOffset.insert(make_pair(name, offsets));
  }
}

void OffsetTable::updateCellSize() {
  cellSize.clear();
  for (int row = 0; row < rowCount(); row++) {
    QTableWidgetItem* itemOrder = item(row, ORDER_COL);
    if (itemOrder == nullptr || itemOrder->text().isEmpty()) break;
    int order = itemOrder->text().toInt();

    QTableWidgetItem* itemSize = item(row, CELL_SIZE_COL);
    if (itemSize == nullptr || itemSize->text().isEmpty()) break;

    float current_size = itemSize->text().toFloat();

    cellSize.insert(make_pair(order, current_size));
  }
}

void OffsetTable::updateFacetDis() {
  facetDis.clear();
  for (int row = 0; row < rowCount(); row++) {
    QTableWidgetItem* itemOrder = item(row, ORDER_COL);
    if (itemOrder == nullptr || itemOrder->text().isEmpty()) break;
    int order = itemOrder->text().toInt();

    QTableWidgetItem* itemSize = item(row, FACET_DIS_COL);
    if (itemSize == nullptr || itemSize->text().isEmpty()) break;

    float current_size = itemSize->text().toFloat();

    facetDis.insert(make_pair(order, current_size));
  }
}

void OffsetTable::updateFacetSize() {
  facetSize.clear();
  for (int row = 0; row < rowCount(); row++) {
    QTableWidgetItem* itemOrder = item(row, ORDER_COL);
    if (itemOrder == nullptr || itemOrder->text().isEmpty()) break;
    int order = itemOrder->text().toInt();

    QTableWidgetItem* itemSize = item(row, FACET_SIZE_COL);
    if (itemSize == nullptr || itemSize->text().isEmpty()) break;

    float current_size = itemSize->text().toFloat();

    facetSize.insert(make_pair(order, current_size));
  }
}

void OffsetTable::updateOrder() {
  regionOrder.clear();

  for (int row = 0; row < rowCount(); row++) {
    QTableWidgetItem* itemName = item(row, NAME_COL);
    if (itemName == nullptr || itemName->text().isEmpty()) break;

    string name = itemName->text().toStdString();

    QTableWidgetItem* itemOrder = item(row, ORDER_COL);
    int order = itemOrder->text().toInt();
    regionOrder.insert(make_pair(order, name));
  }
}

void OffsetTable::on_cell_changed(int row, int col) {
  if (row != -1 && col != -1) {
    updateOffset();
    updateOrder();
    updateCellSize();
    updateFacetDis();
    updateFacetSize();
  }

  // this->resizeColumnsToContents();
}

std::map<std::string, std::array<float, 3>> OffsetTable::GetRegionOffset()
    const {
  return regionOffset;
}

std::map<int, string> OffsetTable::GetRegionOrder() const {
  return regionOrder;
}

std::map<int, float> OffsetTable::GetCellSize() const { return cellSize; }

std::map<int, float> OffsetTable::GetFacetSize() const { return facetSize; }

std::map<int, float> OffsetTable::GetFacetDis() const { return facetDis; }

void OffsetTable::initializeRegions(vector<string> regionNames) {
  // Clear all rows
  while (rowCount() != 0) removeRow(rowCount() - 1);

  // Insert initial regions
  for (unsigned i = 0; i < regionNames.size(); i++) {
    insertRow(i);

    QString qOrder;
    qOrder.asprintf("%d", i);

    setItem(i, ORDER_COL, new QTableWidgetItem(qOrder));
    setItem(i, NAME_COL, new QTableWidgetItem(QString(regionNames[i].c_str())));

    for (int j = OFFSET_X_COL; j <= OFFSET_Z_COL; j++) {
      setItem(i, j, new QTableWidgetItem(QString('0')));
    }
  }

  updateOffset();
  updateOrder();
  updateCellSize();
  updateFacetDis();
  updateFacetSize();
}

void OffsetTable::write_variable_columns(float facet_distance, float facet_size,
                                         float cell_size) {
  std::cout << "writting to table ..." << std::endl;
  for (int i = 0; i < rowCount(); i++) {
    QTableWidgetItem* new_cellsize = new QTableWidgetItem();
    new_cellsize->setText(QString::number(cell_size, 'f', 2));
    setItem(i, CELL_SIZE_COL, new_cellsize);

    QTableWidgetItem* new_facetdis = new QTableWidgetItem();
    new_facetdis->setText(QString::number(facet_distance, 'f', 2));
    setItem(i, FACET_DIS_COL, new_facetdis);

    QTableWidgetItem* new_facetsize = new QTableWidgetItem();
    new_facetsize->setText(QString::number(facet_size, 'f', 2));
    setItem(i, FACET_SIZE_COL, new_facetsize);
  }
}
