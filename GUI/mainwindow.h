#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define QT_NO_KEYWORDS
#include <QMainWindow>
#include <array>
#include <string>
#include <vector>

#include "./Core/MeshProperty.hpp"
#include "filedialog.h"
#include "vtkwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget* parent = 0);
  ~MainWindow();

 private Q_SLOTS:
  void on_actionMesh_triggered();

  void enableSettings(bool);

  void on_applyButton_released();

  void start_mesh();

  void on_actionVisualize_triggered();

  void on_actionSettings_triggered();

  void on_actionAs_settings_triggered();

  void on_actionMesh_Compare_triggered();

  void on_low_resolution_radio_clicked();

  void on_high_resolution_radio_clicked();

  void on_pushButton_released();

 private:
  Ui::MainWindow* ui;

  VTKWindow* vtkWindow = nullptr;

  std::vector<std::string> filePaths;

  QString saveFilePath;

  // Read property settings from line edits
  MeshProperty read_properties_from_lineEdits();

  void write_properties_to_lineEdits(MeshProperty);

  bool isQualityAssessmentEnabled();

  void offSetTableSetup();

  void reOrderFilePaths(std::vector<std::string>& filePaths,
                        const std::map<int, std::string> regionOrder);

  std::array<double, 6> read_bbox_margin();
};

#endif  // MAINWINDOW_H
