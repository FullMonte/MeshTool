#include "mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QFutureWatcher>
#include <QProgressDialog>
#include <QTableWidgetItem>
#include <QTextStream>
#include <QThread>
#include <QTimer>
#include <QtConcurrent/QtConcurrent>
#include <iostream>

#include "CompareMeshDialog.hpp"
#include "Core/DirectoryHelper.hpp"
#include "Core/MeshStarter.hpp"
#include "Core/PolyMaths.hpp"
#include "MessageBoxUtils.h"
#include "offsettable.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  enableSettings(false);
  ui->applyButton->setEnabled(false);
  ui->cellSize_lineEdit->setPlaceholderText(
      "This Cell Size Will Apply To All Layers");
  // ui->actionVisualize->setEnabled(false);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_actionMesh_triggered() {
  filePaths = FileDialog::GetInputFile(this);

  if (filePaths.size() == 0) return;

  saveFilePath.clear();

  enableSettings(true);

  offSetTableSetup();

  ui->applyButton->setEnabled(true);

  ui->pushButton->setEnabled(true);
}

void MainWindow::enableSettings(bool enabled) {
  ui->facetAngle_lineEdit->setEnabled(enabled);
  ui->facetSize_lineEdit->setEnabled(enabled);
  ui->facetDistance_lineEdit->setEnabled(enabled);
  ui->cellSize_lineEdit->setEnabled(enabled);
  ui->cellRadiusEdgeRatio_lineEdit->setEnabled(enabled);
  ui->smooth_lineEdit->setEnabled(enabled);
  ui->bboxMargin_xmax_lineEdit->setEnabled(enabled);
  ui->bboxMargin_ymax_lineEdit->setEnabled(enabled);
  ui->bboxMargin_zmax_lineEdit->setEnabled(enabled);
  ui->bboxMargin_xmin_lineEdit->setEnabled(enabled);
  ui->bboxMargin_ymin_lineEdit->setEnabled(enabled);
  ui->bboxMargin_zmin_lineEdit->setEnabled(enabled);
  ui->low_resolution_radio->setEnabled(enabled);
  ui->high_resolution_radio->setEnabled(enabled);
  ui->qualityAssess_check->setEnabled(enabled);
  ui->FileTypeBox->setEnabled(enabled);
}

void MainWindow::on_applyButton_released() {
  if (filePaths.size() > 0) {
    //        if(saveFilePath.isEmpty())
    saveFilePath = QFileDialog::getSaveFileName(this);

    // If no save path is input, don't do anything
    if (saveFilePath.isEmpty()) return;
  }

  // Disable GUI when meshing
  this->setEnabled(false);

  // Reorder the surface order by the user input
  reOrderFilePaths(filePaths, ui->offsetTable->GetRegionOrder());

  // Start meshing
  start_mesh();

  // Re enabled the GUI
  ui->actionVisualize->setEnabled(true);
  this->setEnabled(true);
}

void MainWindow::on_pushButton_released() {
  float cellSize = ui->cellSize_lineEdit->text().toFloat();
  float facetSize = ui->facetSize_lineEdit->text().toFloat();
  float facetDis = ui->facetDistance_lineEdit->text().toFloat();

  ui->offsetTable->write_variable_columns(facetDis, facetSize, cellSize);
  return;
}

MeshProperty MainWindow::read_properties_from_lineEdits() {
  float facetAngle = ui->facetAngle_lineEdit->text().toFloat();
  float cellRadiusEdgeRatio =
      ui->cellRadiusEdgeRatio_lineEdit->text().toFloat();
  int smoothFactor = ui->smooth_lineEdit->text().toInt();

  // float cellSize = ui->cellSize_lineEdit->text().toFloat();

  std::map<int, float> CellSizeMap, facetDistance, facetSize;

  MeshProperty tmp(facetAngle, facetSize, facetDistance, cellRadiusEdgeRatio,
                   CellSizeMap, smoothFactor, read_bbox_margin());

  // override cellSize with list
  // tmp.cellSizeFromString(cellSizeStr);

  return tmp;
}

void MainWindow::write_properties_to_lineEdits(MeshProperty property) {
  QString qFacetAngle, qFacetSize, qFacetDistance, qCellRadiusEdgeRatio,
      qCellSize, qSmoothFactor;

  qFacetAngle.asprintf("%f", property.facetAngle);
  qFacetSize.asprintf("%f", property.facetSize[0]);
  qFacetDistance.asprintf("%f", property.facetDis[0]);
  qCellRadiusEdgeRatio.asprintf("%f", property.cellRadiusEdgeRatio);
  qSmoothFactor.asprintf("%d", property.smoothFactor);
  qCellSize.asprintf("%d", property.cellSize[0]);

  ui->facetAngle_lineEdit->setText(qFacetAngle);
  ui->facetSize_lineEdit->setText(qFacetSize);
  ui->facetDistance_lineEdit->setText(qFacetDistance);
  ui->cellRadiusEdgeRatio_lineEdit->setText(qCellRadiusEdgeRatio);
  ui->cellSize_lineEdit->setText(qCellSize);
  ui->smooth_lineEdit->setText(qSmoothFactor);
  ui->offsetTable->write_variable_columns(
      property.facetDis[0], property.facetSize[0], property.cellSize[0]);
}

void MainWindow::start_mesh() {
  QFileInfo info(QString::fromStdString(filePaths[0]));
  QString file_extension = info.suffix();

  // Read mesh Parameters
  MeshProperty property = read_properties_from_lineEdits();
  property.perRegionOffset = ui->offsetTable->GetRegionOffset();
  property.cellSize = ui->offsetTable->GetCellSize();
  property.facetDis = ui->offsetTable->GetFacetDis();
  property.facetSize = ui->offsetTable->GetFacetSize();
  property.outFileType = ui->FileTypeBox->currentIndex();

  // Check if input files are all same type
  for (std::string s : filePaths) {
    QFileInfo temp(QString::fromStdString(s));
    QString suffix = temp.suffix();
    if (suffix != file_extension) {
      MessageBoxUtils::showErrorMessageBox(
          QString("Input files are not same type."), this);
      return;
    }
  }

  // Create a progress dialog.
  QProgressDialog dialog(this);
  dialog.setLabelText(QString("Start meshing"));
  dialog.setCancelButton(0);  // Disable the cancel button
  dialog.setWindowFlags(
      Qt::Drawer | Qt::Window | Qt::WindowTitleHint |
      Qt::CustomizeWindowHint);  // Disable the close and resize button

  // Create a QFutureWatcher and connect signals and slots.
  QFutureWatcher<bool> futureWatcher;
  QObject::connect(&futureWatcher, &QFutureWatcher<bool>::finished, &dialog,
                   &QProgressDialog::reset);
  QObject::connect(&futureWatcher, &QFutureWatcher<bool>::progressRangeChanged,
                   &dialog, &QProgressDialog::setRange);
  QObject::connect(&futureWatcher, &QFutureWatcher<bool>::progressValueChanged,
                   &dialog, &QProgressDialog::setValue);

  struct Mesh_function_param {
    std::vector<std::string> filePaths;
    std::string fileType;
    std::string saveFilePath;
    MeshProperty property;
    bool enabledQA;
  };

  // Prepare the vector.
  QVector<Mesh_function_param> param_vec;
  Mesh_function_param m_param = {.filePaths = filePaths,
                                 .fileType = file_extension.toStdString(),
                                 .saveFilePath = saveFilePath.toStdString(),
                                 .property = property,
                                 .enabledQA = isQualityAssessmentEnabled()};
  param_vec.push_back(m_param);

  std::function<bool(Mesh_function_param&)> mesh_function =
      [](Mesh_function_param& param) {
        return mesh_start(param.filePaths, param.fileType, param.saveFilePath,
                          param.property, param.enabledQA);
      };

  // Start the computation.
  futureWatcher.setFuture(QtConcurrent::run(mesh_function, m_param));

  // Display the dialog and start the event loop.
  dialog.exec();

  futureWatcher.waitForFinished();

  bool result = futureWatcher.result();
  if (result) {
    MessageBoxUtils::showInfoMessageBox(
        QString("Mesh is created successfully ! Please use Paraview to check "
                "the result."),
        this);
  } else {
    MessageBoxUtils::showErrorMessageBox(
        QString("Failed to create mesh! Please check the log information."),
        this);
  }
}

void MainWindow::offSetTableSetup() {
  std::vector<std::string> names;
  for (auto name : filePaths) {
    names.push_back(basename(name));
  }
  ui->offsetTable->initializeRegions(names);
}

void MainWindow::on_actionVisualize_triggered() {
  if (vtkWindow == nullptr) {
    vtkWindow = new VTKWindow();
  }

  vtkWindow->readVTKFile((saveFilePath + ".mesh.vtk").toStdString());
  vtkWindow->show();
}

void MainWindow::on_actionSettings_triggered() {
  QString path = QFileDialog::getOpenFileName(this);
  QFile setting(path);

  if (!setting.open(QIODevice::ReadOnly | QIODevice::Text)) return;

  QTextStream in(&setting);
  MeshProperty m_property;
  QString line = in.readLine();
  while (!line.isEmpty()) {
    QStringList ql = line.split(" ", QString::SkipEmptyParts);
    std::string property = ql.at(0).toStdString();
    std::cout << property << std::endl;
    line = in.readLine();

    float value = ql.at(1).toFloat();
    if (property == "facetAngle") {
      m_property.facetAngle = value;
    } else if (property == "cellRadiusEdgeRatio") {
      m_property.cellRadiusEdgeRatio = value;
    } else if (property == "smoothFactor") {
      m_property.smoothFactor = (int)value;
    }
  }

  write_properties_to_lineEdits(m_property);
  setting.close();
}

void MainWindow::on_actionAs_settings_triggered() {
  QString outPath = QFileDialog::getSaveFileName(this);

  QFile out(outPath);
  if (!out.open(QIODevice::WriteOnly | QIODevice::Text)) return;

  QTextStream s(&out);

  // Read mesh parameters
  MeshProperty property = read_properties_from_lineEdits();

  s << "facetAngle " << property.facetAngle << Qt::endl;
  s << "cellRadiusEdgeRatio " << property.cellRadiusEdgeRatio << Qt::endl;

  // display list of cell sizes (could be single item)
  s << "cellSizes: ";
  for (size_t i = 0; i < filePaths.size(); i++) {
    s << property.cellSize[i] << ", ";
  }

  s << "facetSizes: ";
  for (size_t i = 0; i < filePaths.size(); i++) {
    s << property.facetSize[i] << ", ";
  }

  s << "facetDistances: ";
  for (size_t i = 0; i < filePaths.size(); i++) {
    s << property.facetDis[i] << ", ";
  }

  s << "\n";
  out.close();
}

void MainWindow::reOrderFilePaths(
    std::vector<std::string>& filePaths,
    const std::map<int, std::string> regionOrder) {
  std::vector<std::string> copy;

  for (auto iter = regionOrder.begin(); iter != regionOrder.end(); iter++) {
    for (unsigned i = 0; i < filePaths.size(); i++) {
      if (iter->second == basename(filePaths[i])) copy.push_back(filePaths[i]);
    }
  }

  filePaths = copy;
}

bool MainWindow::isQualityAssessmentEnabled() {
  return ui->qualityAssess_check->isChecked();
}

std::array<double, 6> MainWindow::read_bbox_margin() {
  std::array<double, 6> margin;

  margin[0] = ui->bboxMargin_xmin_lineEdit->text().toDouble();
  margin[1] = ui->bboxMargin_ymin_lineEdit->text().toDouble();
  margin[2] = ui->bboxMargin_zmin_lineEdit->text().toDouble();
  margin[3] = ui->bboxMargin_xmax_lineEdit->text().toDouble();
  margin[4] = ui->bboxMargin_ymax_lineEdit->text().toDouble();
  margin[5] = ui->bboxMargin_zmax_lineEdit->text().toDouble();

  return margin;
}

void MainWindow::on_actionMesh_Compare_triggered() {
  std::pair<std::string, std::string> files = CompareMeshDialog::GetFiles(this);
  if (!files.first.empty() && !files.second.empty())
    compareUnstructuredGrid(files.first, files.second);
}

void MainWindow::on_low_resolution_radio_clicked() {
  std::map<int, float> cellSizeResLow;
  std::map<int, float> facetSizeResLow;
  std::map<int, float> facetDisResLow;

  for (size_t i = 0; i < filePaths.size(); i++) {
    cellSizeResLow[i] = 7.0;
    facetSizeResLow[i] = 5.0;
    facetDisResLow[i] = 0.1;
  }

  ui->high_resolution_radio->setChecked(false);
  MeshProperty low_resolution(25, facetSizeResLow, facetDisResLow, 1.5,
                              cellSizeResLow, 200, {0, 0, 0, 0, 0, 0});
  write_properties_to_lineEdits(low_resolution);
}

void MainWindow::on_high_resolution_radio_clicked() {
  std::map<int, float> cellSizeResHigh;
  std::map<int, float> facetSizeResHigh;
  std::map<int, float> facetDisResHigh;

  for (size_t i = 0; i < filePaths.size(); i++) {
    cellSizeResHigh[i] = 4.0;
    facetSizeResHigh[i] = 3.0;
    facetDisResHigh[i] = 0.07;
  }

  ui->low_resolution_radio->setChecked(false);
  MeshProperty high_resolution(25, facetSizeResHigh, facetDisResHigh, 1.5,
                               cellSizeResHigh, 200, {0, 0, 0, 0, 0, 0});
  write_properties_to_lineEdits(high_resolution);
}
