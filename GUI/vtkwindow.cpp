#include "vtkwindow.h"
#include "ui_vtkwindow.h"
#include <vtkwindow.h>

using namespace std;

VTKWindow::VTKWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VTKWindow)
{
    ui->setupUi(this);

    actor = vtkActor::New();
    reader = vtkUnstructuredGridReader::New();
    renderer = vtkOpenGLRenderer::New();


}

VTKWindow::~VTKWindow()
{
    delete ui;

    actor->Delete();
    renderer->Delete();
    reader->Delete();
    if(clipper != nullptr)
        delete clipper;
}


void VTKWindow::readVTKFile(string filePath){
    reader->SetFileName(filePath.c_str());
    if(clipper != nullptr){
        clipper->Delete();
        delete clipper;
        clipper = nullptr;
    }

//    reader->SetFileName("/home/zach/MeshTool/mesh-result/Fat.mesh.vtk");
    reader->Update();
    vtkDataSetMapper* mapper = vtkDataSetMapper::New();
    mapper->SetInputConnection(reader->GetOutputPort());

    actor->SetMapper(mapper);

    renderer->AddActor(actor);
    renderer->ResetCamera();

    ui->vtkWidget->GetRenderWindow()->AddRenderer(renderer);

    ui->vtkWidget->GetRenderWindow()->Render();

}



void VTKWindow::on_actionClipper_triggered()
{

    if(clipper == nullptr){
        clipper = new Clipper;
        clipper->createClipper(actor,
                            ui->vtkWidget->GetRenderWindow()->GetInteractor(),
                            reader->GetOutput());
    }

    ui->vtkWidget->GetRenderWindow()->Render();
}
