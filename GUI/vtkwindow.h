#ifndef VTKWINDOW_H
#define VTKWINDOW_H

#include <vtkAutoInit.h>

#include <QMainWindow>
#include <string>
VTK_MODULE_INIT(vtkInteractionStyle)

#include <vtkActor.h>
#include <vtkDataSetMapper.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkOpenGLRenderer.h>
#include <vtkUnstructuredGridReader.h>

#include "./VTK/Clipper.h"

namespace Ui {
class VTKWindow;
}

class VTKWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit VTKWindow(QWidget* parent = 0);
  ~VTKWindow();

  void readVTKFile(std::string);

 private Q_SLOTS:
  void on_actionClipper_triggered();

 private:
  Ui::VTKWindow* ui;

  vtkActor* actor;

  vtkOpenGLRenderer* renderer;

  vtkUnstructuredGridReader* reader;

  Clipper* clipper = nullptr;
};

#endif  // VTKWINDOW_H
