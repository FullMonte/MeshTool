/*
 * VTKLegacyReader.cpp
 *
 *  Created on: May 27, 2015
 *      Author: jcassidy
 */

#include "VTKLegacyReader.hpp"
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkCellTypes.h>
#include <vtkCellData.h>

VTKLegacyReader::~VTKLegacyReader()
{

}

#include <utility>
#include <array>
#include <string>
#include <iostream>
#include <vtkPolyDataReader.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkCell.h>
#include <vtkTriangleFilter.h>
#include <vtkSmoothPolyDataFilter.h>

#include <boost/range/algorithm.hpp>
using namespace std;

/** NOTE: Will prepend an additional point and/or cell if m_addZeroPoint and/or m_addZeroCell are set.
 *
 * Typically FullMonte uses the zero point and zero tetra as special dummy variables.
 * If the mesh was saved from FullMonte, they should already be in place (can set options to false).
 *
 * Coming from other sources, there typically will not be such points added so it should be set true.
 *
 */

pair<vector<array<float,3>>,vector<array<unsigned,3>>> VTKLegacyReader::readSurface(const char* fn, int smoothFactor)
{

    m_addZeroPoint = false;
    m_addZeroCell = false;

	vtkPolyDataReader* R = vtkPolyDataReader::New();
		R->SetFileName(fn);
		R->Update();

    //Filter the data to pure triangles
    vtkTriangleFilter* triFilter = vtkTriangleFilter::New();
    triFilter->SetInputData(R->GetOutput());
    triFilter->Update();
//    vtkPolyData* pd = triFilter->GetOutput();


    //Surface smoothing
    cout << "Smoothing the surface " << endl;
    vtkSmoothPolyDataFilter* smoothFilter = vtkSmoothPolyDataFilter::New();
    smoothFilter->SetInputData(triFilter->GetOutput());
    smoothFilter->SetNumberOfIterations(smoothFactor);
    smoothFilter->Update();
    vtkPolyData* pd = smoothFilter->GetOutput();

	cout << "Read a surface mesh with " << pd->GetNumberOfPoints() << " points and " << pd->GetNumberOfPolys() << " triangles" << endl;

	bool usePointData=false;

	if (usePointData)
	{
		vtkPointData* pdata = pd->GetPointData();
		vtkFloatArray* ca = vtkFloatArray::SafeDownCast(pdata->GetScalars());

		cout << "  Surface has " << ca->GetNumberOfTuples() << " scalars in its point data array" << endl;
	}
	

	vector<array<float,3>> P(pd->GetNumberOfPoints()+(m_addZeroPoint?1:0));
    vector<array<unsigned,3>> T(pd->GetNumberOfPolys()+(m_addZeroCell?1:0));

	if (m_addZeroPoint)
		P[0] = array<float,3>{0,0,0};

	if(m_addZeroCell)
		T[0] = array<unsigned,3>{0,0,0};

	
	cout << "convert points" << endl;

	for(unsigned i=0;i<pd->GetNumberOfPoints();++i)
	{
		double *p = pd->GetPoint(i);
		std::copy(p, p+3, P[i+m_addZeroPoint].data());
	}

	for(unsigned i=0;i<pd->GetNumberOfPolys();++i)
	{
		vtkCell* c = pd->GetCell(i);
		if (c->GetNumberOfPoints() != 3)
			cout << "ERROR: Expecting 3 points in a cell, received " << c->GetNumberOfPoints() << endl;
		else
			for(unsigned j=0;j<3;++j)
				T[i+m_addZeroCell][j] = c->GetPointId(j)+m_addZeroPoint;
	}
	
	cout << "converted done" << endl;

    triFilter->Delete();

	return make_pair(P,T);
}


