#ifndef IMPLICITPLANECALLBACK_H
#define IMPLICITPLANECALLBACK_H

#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkClipDataSet.h>
#include <vtkClipVolume.h>
#include <vtkDataSetMapper.h>
#include <vtkImplicitPlaneWidget.h>
#include <vtkPlane.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProjectedTetrahedraMapper.h>
#include <vtkSmartVolumeMapper.h>
#include <vtkExtractGeometry.h>

class VTKImplicitPlaneWidgetCall : public vtkCommand
{
	//this is the call back class we use to get the clipped data every time 
public:

	static VTKImplicitPlaneWidgetCall *New()
	{
		return new VTKImplicitPlaneWidgetCall;
	}
public:
	virtual void Execute(vtkObject *caller, unsigned long eventId, void *callData)
	{
		vtkImplicitPlaneWidget *pWidget = reinterpret_cast<vtkImplicitPlaneWidget*>(caller);
		{
			// update the clip plane and the second renderer
			pWidget->GetPlane(pPlane);
			
            dataset->Update();
        }

	}
    void setDataset(vtkUnstructuredGridAlgorithm* e){ dataset = e; }

    void setPlane(vtkPlane* other){ pPlane = other; }


private:
	vtkSmartPointer<vtkPlane> pPlane;

    vtkUnstructuredGridAlgorithm* dataset;

};






#endif 
