#### Build VTK-7.1.1
FROM ubuntu:18.04 as vtk-7.1.1-build

# Download VTK-7.1.1 source
RUN apt clean
RUN apt-get update && apt-get install -y curl
WORKDIR /build
RUN curl -L https://www.vtk.org/files/release/7.1/VTK-7.1.1.tar.gz -o VTK-7.1.1.tar.gz
RUN tar -zxf VTK-7.1.1.tar.gz

# Install build tools
RUN apt-get update -y && apt-get install -y g++ ninja-build cmake libgl-dev libx11-dev libxt-dev qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools libqt5x11extras5-dev qttools5-dev

# OpenGL is the older, less preferred backend but seems to be necessary for Qt support
# Module_vtkGUISupportQtOpenGL required for QVTKWidget2.h
# Build it
WORKDIR /build/VTK-7.1.1/Build/Release
RUN cmake -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr/local/VTK-7.1.1 \
    -DQt5_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5 \
    -DVTK_QT_VERSION=5 \
    -DVTK_Group_Rendering=ON \
    -DVTK_Group_StandAlone=ON \
    -DModule_vtkGUISupportQtOpenGL=ON \
    -DVTK_Group_Qt=ON \
    -DVTK_Group_Tk=OFF \
    -DVTK_USE_CXX11_FEATURES=ON \
    -DVTK_RENDERING_BACKEND=OpenGL \
    -DVTK_WRAP_TCL=OFF \
    -DVTK_WRAP_PYTHON=OFF \
    -DVTK_WRAP_JAVA=OFF \
    -DVTK_USE_X=ON \
    /build/VTK-7.1.1
RUN ninja -j8
RUN ninja -j8
RUN ninja install


#### Build ITKsnap-3.8.0
FROM ubuntu:18.04 as itksnap-3.8.0

# Download VTK-7.1.1 source
RUN apt-get update -y && apt-get install -y wget tar
WORKDIR /itksnap/the/workdir/path
RUN wget https://sourceforge.net/projects/itk-snap/files/itk-snap/3.8.0/itksnap-3.8.0-20190612-Linux-x86_64-qt4.tar.gz
RUN tar -zxf itksnap-3.8.0-20190612-Linux-x86_64-qt4.tar.gz
RUN mv itksnap-3.8.0-20190612-Linux-gcc64-qt4 ITKsnap-3.8.0


#### Development environment image
#
# Provides all the tools (CMake, Ninja, G++, VTK with headers, CGAL with headers, etc) to build

FROM ubuntu:24.04 as dev

# Avoid prompts
ENV DEBIAN_FRONTEND=noninteractive

# Copy VTK-7.1.1 install (with libs & includes)
COPY --from=VTK-7.1.1-build /usr/local/VTK-7.1.1 /usr/local/VTK-7.1.1

#Install build tools & dependencies
RUN rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*.deb && \
    apt-get update -y && \
    apt-get install -y ninja-build gcc g++ libeigen3-dev libgl1 libxt-dev libsm-dev libqt5x11extras5-dev libgmp-dev qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools libmpfr-dev libtool libssl-dev libtbb-dev

#RUN apt-get update  --fix-missing -y && apt-get install -y ninja-build  gcc g++ libeigen3-dev libgl1 libxt-dev libsm-dev libqt5x11extras5-dev  libgmp-dev  qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools libmpfr-dev libtool libssl-dev libtbb-dev 
RUN apt-get install -y cmake libboost-all-dev

#download CGAL-5.6.1 from source 
RUN apt-get update --fix-missing -y && apt-get install -y curl wget
RUN mkdir -p build && \
    cd /build && \
    curl -L https://github.com/CGAL/cgal/archive/refs/tags/v5.6.1.tar.gz -o CGAL-5.6.1.tar.gz && \
    tar -zxf CGAL-5.6.1.tar.gz && \
    cd /build/cgal-5.6.1 && \
    cmake . && \
    make install
WORKDIR /builds/FullMonte/MeshTool
ENV LD_LIBRARY_PATH=/usr/local/VTK-7.1.1/lib:/usr/local/MeshTool/lib:usr/lib/x86_64-linux-gnu:/usr/local/ITKsnap/lib

#### Run-time image
#
# Installs run-time dependencies, including VTK and Qt
# Sets environment (PATH, LD_LIBRARY_PATH)
# Copies build context (need to provide as install dir) to /usr/local/MeshTool

FROM ubuntu:24.04 as run

RUN apt-get update -y --fix-missing && apt-get install -y libboost-all-dev libxt6 libx11-6 libqt5x11extras5  libcurl4t64 libglu1-mesa libxi-dev libxmu-dev libglu1-mesa-dev libtbb-dev libgmp-dev libmpfr-dev  qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools
# Install Paraview
RUN apt-get update -y --fix-missing && apt-get install -y paraview

COPY --from=vtk-7.1.1-build /usr/local/VTK-7.1.1/lib            /usr/local/VTK-7.1.1/lib
COPY --from=vtk-7.1.1-build /usr/lib/x86_64-linux-gnu/libQt5*   /usr/local/Qt/lib/
COPY --from=itksnap-3.8.0 /itksnap/the/workdir/path/ITKsnap-3.8.0/bin            /usr/local/ITKsnap/bin
COPY --from=itksnap-3.8.0 /itksnap/the/workdir/path/ITKsnap-3.8.0/lib            /usr/local/ITKsnap/lib
COPY --from=dev /build/cgal-5.6.1                               /usr/local/Cgal
COPY bin/                                                       /usr/local/MeshTool/bin
COPY lib/                                                       /usr/local/MeshTool/lib
ENV PATH=$PATH:/usr/local/MeshTool/bin:/usr/local/ITKsnap/bin
ENV LD_LIBRARY_PATH=/usr/local/VTK-7.1.1/lib:/usr/local/MeshTool/lib:usr/lib/x86_64-linux-gnu:/usr/local/ITKsnap/lib
RUN ln usr/lib/x86_64-linux-gnu/libpng16.so.16.43.0 usr/lib/x86_64-linux-gnu/libpng12.so.0
RUN ln usr/lib/x86_64-linux-gnu/libicuuc.so.74.2  usr/lib/x86_64-linux-gnu/libicuuc.so.55 
RUN ln usr/lib/x86_64-linux-gnu/libicuio.so.74.2  usr/lib/x86_64-linux-gnu/libicui18n.so.55
RUN ln usr/lib/x86_64-linux-gnu/libpcre2-16.so.0.11.2  usr/lib/x86_64-linux-gnu/libpcre16.so.3 
RUN ln usr/lib/x86_64-linux-gnu/libicui18n.so.74.2  usr/lib/x86_64-linux-gnu/libicui18n.so.60
RUN ln  usr/lib/x86_64-linux-gnu/libdouble-conversion.so.3.1  usr/lib/x86_64-linux-gnu/libdouble-conversion.so.1
