#ifndef POLYMATHS_HPP
#define POLYMATHS_HPP
#include <CGAL/Kernel_traits.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT FT;
typedef K::Point_3 Point_3;
typedef FT (Function)(const Point_3&);
typedef CGAL::Exact_predicates_exact_constructions_kernel Exact_kernal;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef CGAL::Polyhedron_3<Exact_kernal> Exact_polyhedron;

//Return the difference percentagle of the origin polyhedron to another polyhedron
// %difference = 1 - Union(pOrigin - pOther)/pOrigin
FT differenceBetweenPolyhedron(const Polyhedron& pTest,const Polyhedron& pRef);

FT computeVolume (const Polyhedron& p);


#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <string>

void compareUnstructuredGrid(std::string test,std::string ref);

double volumeDiffBetweenMesh(vtkUnstructuredGrid* ug1, vtkUnstructuredGrid* ug2 );

double volumeDiffBetweenSurface(vtkPolyData* test, vtkPolyData* ref );


#endif // POLYMATHS_HPP
