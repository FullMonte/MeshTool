#ifndef MESHPROPERTY_HPP
#define MESHPROPERTY_HPP

#include <array>
#include <exception>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

struct MeshProperty {
  float facetAngle;
  std::map<int, float> facetSize;
  std::map<int, float> facetDis;
  float cellRadiusEdgeRatio;
  std::map<int, float> cellSize;
  int smoothFactor;
  std::array<double, 6> bbox_margin;  // (xmin,ymin,zmin,xmax,ymax,zmax)
  std::map<std::string, std::array<float, 3>> perRegionOffset;
  int outFileType;

  MeshProperty(float facet_angle = 0.0,
               std::map<int, float> facet_size = std::map<int, float>(),
               std::map<int, float> facet_dis = std::map<int, float>(),
               float cell_radius_edge_ratio = 0.0,
               std::map<int, float> cell_size = std::map<int, float>(),
               int smooth_factor = 0,
               std::array<double, 6> bboxMargin = {0, 0, 0, 0, 0, 0})
      : facetAngle(facet_angle),
        facetSize(facet_size),
        facetDis(facet_dis),
        cellRadiusEdgeRatio(cell_radius_edge_ratio),
        cellSize(cell_size),
        smoothFactor(smooth_factor),
        bbox_margin(bboxMargin),
        outFileType(0) {}  // default to ASCII file type
};
#endif  // MESHPROPERTY_HPP
