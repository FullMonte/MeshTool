#ifndef MESHER_HPP
#define MESHER_HPP

#include <string>
#include <vector>

// using namespace std;

#include "MeshProperty.hpp"

class Mesher {
 public:
  // Read from the files and prepare the data before meshing
  // Return false if the input files are invalid
  virtual bool read(std::vector<std::string> files) = 0;

  // Return true if the meshing succeeded
  virtual bool mesh(MeshProperty) = 0;

  // Write to vtk unstructured grid type
  virtual bool writeToVtkUnstructuredGrid(std::string, MeshProperty) = 0;

  // Print the quality accessment of the mesh
  virtual void qualityAccess() = 0;
};

#endif  // MESHER_HPP
