#ifndef CGALMESHER_HPP
#define CGALMESHER_HPP

#include <CGAL/Bbox_3.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Implicit_to_labeling_function_wrapper.h>
#include <CGAL/Labeled_mesh_domain_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_constant_domain_field_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Polygon_mesh_processing/bbox.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/alpha_wrap_3.h>
#include <CGAL/optimal_bounding_box.h>
#include <vtkPolyData.h>

#include <array>
#include <boost/range/adaptor/indexed.hpp>
#include <string>
#include <vector>

#include "Labeled_mesh_domain_with_custom_seeds.hpp"
#include "MeshProperty.hpp"
#include "Mesher.hpp"
#include "Polyhedral_mesh_domain_with_custom_seeds.hpp"

// #define CGAL_MESH_3_VERBOSE

/*
 *  A labeling function for CGAL polyhedron surface vector
 */
template <class Polyhedral_function, class BGT>
class Polyhedral_vector_to_labeled_function_wrapper {
 public:
  // Types
  typedef int return_type;
  typedef Polyhedral_function Function;
  typedef std::vector<Function*> Function_vector;
  typedef typename BGT::Point_3 Point_3;
  /// Constructor
  Polyhedral_vector_to_labeled_function_wrapper(std::vector<Function*>& v)
      : function_vector_(v) {}

  /// Destructor
  ~Polyhedral_vector_to_labeled_function_wrapper() {}

  /// Operator ()
  return_type operator()(const Point_3& p, const bool = true) const {
    for (const auto& f : function_vector_ | boost::adaptors::indexed(1U))
      if (f.value()->is_in_domain_object()(p) > 0) return f.index();

    return 0;
  }

 private:
  /// Functions to wrap
  Function_vector function_vector_;
};

/*
 * The class to generate mesh using CGAL libraries.
 */

class CGALMesher : public Mesher {
 public:
  CGALMesher();
  ~CGALMesher();

  /* ----- CGAL properties definations ----*/
  typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
  typedef CGAL::Polyhedron_3<K> Polyhedron;
  typedef CGAL::Polyhedron_3<K>::Vertex Vertex;
  typedef Polyhedral_mesh_domain_with_custom_seeds<Polyhedron, K>
      Polyhedral_mesh_domain;
  typedef Polyhedral_vector_to_labeled_function_wrapper<Polyhedral_mesh_domain,
                                                        K>
      Labeling_func;
  typedef Labeled_mesh_domain_with_custom_seeds<Labeling_func, K> Mesh_domain;
  typedef CGAL::Mesh_constant_domain_field_3<Mesh_domain::R, Mesh_domain::Index>
      Sizing_field;
#ifdef CGAL_CONCURRENT
  typedef CGAL::Mesh_triangulation_3<
      Mesh_domain,
      CGAL::Kernel_traits<Mesh_domain>::Kernel,  // Same as sequential
      CGAL::Parallel_tag                         // Tag to activate parallelism
      >::type Tr;
#else
  typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
#endif
  typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
  typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> Mesh3D;
  typedef std::array<K::Point_3, 8> Obb_array;
  /*-----------------------------------------*/

  // Generate the polyhedron surfaces from vtk files
  // Return false if any of the files is invalid
  bool read(std::vector<std::string> files);

  // The actual mesh function to generate the mesh object
  // Return true if the meshing succeeded
  bool mesh(MeshProperty);

  bool writeToVtkUnstructuredGrid(std::string outputPath,
                                  MeshProperty property);

  // Write polyhedron surfaces for debug purpose
  bool writeToVtkPolyData(std::string outputPath);

  // Print the quality accessment of the mesh
  void qualityAccess();

 private:
  // The name for the regions
  std::vector<std::string> regionNames;

  std::vector<vtkPolyData*> vtkPolyDatas;

  // Vector to store the polyhedrons surfaces from the inner most surface to
  // outer most surface
  std::vector<Polyhedron> polySurfaces;

  // The triangulated mesh generated
  Mesh3D::Triangulation triMesh;

  // Store the outputFile path
  std::string outputFile;

  // Store the mesh quality
  MeshProperty meshProperty;

  // Add a bounding box around all the surfaces before meshing
  void addBoundingBox(MeshProperty);

  // Smooth the surfaces before meshing
  void smoothSurface(vtkPolyData*&, int);

  // Build the CGAL polyhedron from vtkPolyData
  bool buildPolyhedron(Polyhedron& polyhedron, vtkPolyData* polyData,
                       std::array<float, 3> offset);

  // Close the polyhedron surface
  bool closePolyhedron(Polyhedron& polyhedron);
};

// A bounding box polyhedron for CGAL meshing
struct Vertex_bbox {
  struct Construct_bbox_3 {
    CGAL::Bbox_3 operator()(const CGALMesher::Vertex& V) {
      CGALMesher::K::Point_3 point = V.point();
      return point.bbox();
    }
  };

  Construct_bbox_3 construct_bbox_3_object() const {
    return Construct_bbox_3();
  }
};

struct Polyhedron_bbox {
  struct Construct_bbox_3 {
    CGAL::Bbox_3 operator()(const CGALMesher::Polyhedron& P) {
      CGALMesher::Obb_array obb_points;
      CGAL::oriented_bounding_box(P, obb_points);
      return CGAL::Bbox_3(obb_points[0].x(), obb_points[0].y(),
                          obb_points[0].z(), obb_points[7].x(),
                          obb_points[7].y(), obb_points[7].z());
    }
  };
  Construct_bbox_3 construct_bbox_3_object() const {
    return Construct_bbox_3();
  }
};

struct Polydomain_bbox {
  struct Construct_bbox_3 {
    CGAL::Bbox_3 operator()(CGALMesher::Polyhedral_mesh_domain*& P) {
      return P->bbox();
    }
  };
  Construct_bbox_3 construct_bbox_3_object() const {
    return Construct_bbox_3();
  }
};

#endif  // CGALMESHER_HPP
