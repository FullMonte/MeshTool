#ifndef DIRECTORYHELPER_HPP
#define DIRECTORYHELPER_HPP
#include <string>

std::string basename(const std::string s);

std::string dir(std::string s);

#endif // DIRECTORYHELPER_HPP
