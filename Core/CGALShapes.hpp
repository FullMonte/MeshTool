#ifndef CGALSHAPES_HPP
#define CGALSHAPES_HPP

#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>

template <class Poly,class BBox>
Poly make_cube_3(BBox bb) {
    // appends a cube of size [0,1]^3 to the polyhedron P.
    Poly P;
    CGAL_precondition( P.is_valid());
    typedef typename Poly::Point_3         Point;
    typedef typename Poly::Halfedge_handle Halfedge_handle;
    Halfedge_handle h = P.make_tetrahedron( Point( bb.xmax(), bb.ymin(), bb.zmin()),
                                            Point( bb.xmin(), bb.ymin(), bb.zmax()),
                                            Point( bb.xmin(), bb.ymin(), bb.zmin()),
                                            Point( bb.xmin(), bb.ymax(), bb.zmin()) );
    Halfedge_handle g = h->next()->opposite()->next();             // Fig. (a)
    P.split_edge( h->next());
    P.split_edge( g->next());
    P.split_edge( g);                                              // Fig. (b)
    h->next()->vertex()->point()     = Point( bb.xmax(), bb.ymin(), bb.zmax());
    g->next()->vertex()->point()     = Point( bb.xmin(), bb.ymax(), bb.zmax());
    g->opposite()->vertex()->point() = Point( bb.xmax(), bb.ymax(), bb.zmin());            // Fig. (c)
    Halfedge_handle f = P.split_facet( g->next(),
                                       g->next()->next()->next()); // Fig. (d)
    Halfedge_handle e = P.split_edge( f);
    e->vertex()->point() = Point( bb.xmax(), bb.ymax(), bb.zmax());                        // Fig. (e)
    P.split_facet( e, f->next()->next());                          // Fig. (f)

    CGAL_postcondition( P.is_valid());

    CGAL::Polygon_mesh_processing::triangulate_faces(P);
    assert(P.is_pure_triangle());

    return P;
}

#endif 
