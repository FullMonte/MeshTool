/*
 * CGALTriangulationToPT.hpp
 *
 *  Created on: Mar 20, 2016
 *      Author: jcassidy
 */

#ifndef CGALTRIANGULATIONTOPT_HPP_
#define CGALTRIANGULATIONTOPT_HPP_

template<class Triangulation>std::pair<std::vector<std::array<float,3>>,std::vector<std::array<unsigned,4>>>
	CGALTriangulationToPT(const Triangulation& T);

#include <vector>
#include <array>

#include <boost/range.hpp>
#include <unordered_map>

template<typename T,class Alloc=std::allocator<T>>T& auto_access(std::vector<T,Alloc>& v,std::size_t i)
{
	if (i >= v.size())
		v.resize(i+1,T());

	return v[i];
}

template<class Triangulation>std::pair<std::vector<std::array<float,3>>,std::vector<std::array<unsigned,4>>>
	CGALTriangulationToPT(const Triangulation& T)
{
	size_t Np = T.number_of_vertices();
	auto PR = boost::iterator_range<typename Triangulation::Point_iterator>(T.points_begin(),T.points_end());

	std::vector<std::array<float,3>> P;
	std::unordered_map<std::array<float,3>,unsigned,boost::hash<std::array<float,3>>> Pmap;

	P.reserve(Np);
	Pmap.reserve(Np);

	for(const auto  p : PR)
	{
		std::array<float,3> Pf{ float(p[0]),float(p[1]),float(p[2]) };

		if (!Pmap.insert(make_pair(Pf,P.size())).second)
			throw std::logic_error("Failed to insert point (duplicate?)");

		P.push_back(Pf);
	}

	size_t Nt = T.number_of_cells();

	std::vector<std::array<unsigned,4>> tet;

	tet.reserve(Nt);

	auto TR = boost::iterator_range<typename Triangulation::Finite_cells_iterator>(T.finite_cells_begin(),T.finite_cells_end());

	std::vector<unsigned> subdomainMembers;

	for(const auto t : TR)
	{
		auto_access(subdomainMembers,t.subdomain_index())++;
		if (t.subdomain_index())
		{
			std::array<unsigned,4> IDps;

			for(unsigned i=0;i<4;++i)
			{
				typename Triangulation::Point p = t.vertex(i)->point();
				std::array<float,3> pf{float(p[0]),float(p[1]),float(p[2])};
				IDps[i] = Pmap.at(pf);
			}

			tet.push_back(IDps);
		}
	}

	std::cout << "Conversion returned " << P.size() << " points and " << tet.size() << " tetras" << std::endl;

	for(unsigned i=0;i<subdomainMembers.size();++i)
		std::cout << "  Subdomain " << i << ": " << subdomainMembers[i] << std::endl;

	return make_pair(P,tet);
}



#endif /* CGALTRIANGULATIONTOPT_HPP_ */
