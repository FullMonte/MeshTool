#include "CGALMesher.hpp"

#include <CGAL/Polygon_mesh_processing/triangulate_hole.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/make_mesh_3.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkTriangleFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridWriter.h>

using namespace CGAL::parameters;

#include <boost/log/trivial.hpp>
#include <sstream>

#include "CGALMeshToVTKUnstructuredGrid.hpp"
#include "CGALPolyhedronSurfaceToPolyData.hpp"
#include "CGALShapes.hpp"
#include "DirectoryHelper.hpp"
#include "MeshQuality.hpp"
#include "PolyhedronFromPT.hpp"
#include "VTKPolyToCGAL.hpp"

#define ASCII_FILE 0
#define BINARY_FILE 1

CGALMesher::CGALMesher() {}

CGALMesher::~CGALMesher() {
  for (unsigned i = 0; i < vtkPolyDatas.size(); i++) {
    if (vtkPolyDatas[i] != NULL) vtkPolyDatas[i]->Delete();
  }
}

// Generate the polyhedron surfaces from vtk files
// Return false if any of the files is invalid
bool CGALMesher::read(std::vector<std::string> files) {
  // Clear the old data
  regionNames.clear();
  vtkPolyDatas.clear();
  polySurfaces.clear();

  vtkPolyDataReader* R = vtkPolyDataReader::New();
  vtkTriangleFilter* triFilter = vtkTriangleFilter::New();

  // Read in the new vtk poly data
  for (unsigned i = 0; i < files.size(); i++) {
    BOOST_LOG_TRIVIAL(trace) << "Read the file from " << files[i] << endl;

    regionNames.push_back(basename(files[i]));

    // Read data from the given path
    const char* fn = files[i].c_str();
    R->SetFileName(fn);
    R->Update();

    // If it is not a valid vtk poly data, return false
    if (!R->IsFilePolyData()) return false;

    // Triangulate the poly data
    triFilter->SetInputData(R->GetOutput());
    triFilter->Update();
    BOOST_LOG_TRIVIAL(info)
        << "Number of points " << R->GetOutput()->GetNumberOfPoints();
    BOOST_LOG_TRIVIAL(info)
        << "Number of cells " << R->GetOutput()->GetNumberOfCells();

    vtkPolyData* polyData = vtkPolyData::New();
    polyData->DeepCopy(triFilter->GetOutput());

    vtkPolyDatas.push_back(polyData);
  }

  triFilter->Delete();
  R->Delete();

  return true;
}

void CGALMesher::smoothSurface(vtkPolyData*& polyData, int smoothFactor) {
  BOOST_LOG_TRIVIAL(trace) << "Smoothing the surface with factor "
                           << smoothFactor;

  vtkSmoothPolyDataFilter* smoothFilter = vtkSmoothPolyDataFilter::New();

  vtkPolyData* smoothPoly = vtkPolyData::New();
  smoothFilter->SetInputData(polyData);
  smoothFilter->SetNumberOfIterations(smoothFactor);
  smoothFilter->SetOutput(smoothPoly);
  smoothFilter->Update();

  polyData->Delete();
  polyData = smoothPoly;
  smoothFilter->Delete();
}

void CGALMesher::addBoundingBox(MeshProperty property) {
  BOOST_LOG_TRIVIAL(trace) << "Adding the bounding box";

  CGAL::Bbox_3 bb =
      bbox_3(polySurfaces.begin(), polySurfaces.end(), Polyhedron_bbox());
  bb = CGAL::Bbox_3(
      bb.xmin() - property.bbox_margin[0], bb.ymin() - property.bbox_margin[1],
      bb.zmin() - property.bbox_margin[2], bb.xmax() + property.bbox_margin[3],
      bb.ymax() + property.bbox_margin[4], bb.zmax() + property.bbox_margin[5]);

  // Add a bounding box if user required
  Polyhedron bounding_cube;
  bounding_cube = make_cube_3<Polyhedron, CGAL::Bbox_3>(bb);

  polySurfaces.push_back(bounding_cube);
}

bool CGALMesher::buildPolyhedron(CGALMesher::Polyhedron& polyhedron,
                                 vtkPolyData* polyData,
                                 std::array<float, 3> offset) {
  BOOST_LOG_TRIVIAL(trace) << "Building the polyhedron from the vtk polydata ";

  // Convert to points and tetras from VTK poly Data
  if (polyData == NULL) {
    BOOST_LOG_TRIVIAL(warning) << "Empty poly Data";
  } else {
    BOOST_LOG_TRIVIAL(info)
        << "Number of points " << polyData->GetNumberOfPoints();
    BOOST_LOG_TRIVIAL(info)
        << "Number of cells " << polyData->GetNumberOfCells();
  }

  std::pair<std::vector<std::array<float, 3>>,
            std::vector<std::array<unsigned, 3>>>
      PT = VTKPolyToPT(polyData);
  if (PT.first.size() == 0 || PT.second.size() == 0) return false;

  // Adjust offsets
  if (offset[0] != 0.0f || offset[1] != 0.0f || offset[2] != 0.0f) {
    BOOST_LOG_TRIVIAL(info) << "Region adds offset (" << offset[0] << ","
                            << offset[1] << "," << offset[2] << ")";
    for (auto& p : PT.first) {
      p[0] += offset[0];
      p[1] += offset[1];
      p[2] += offset[2];
    }
  }

  typedef CGAL::Polyhedron_3<K>::HalfedgeDS HDS;
  PolyhedronFromPT<HDS> pt(PT.first, PT.second);

  polyhedron = CGAL::Polyhedron_3<K>();
  polyhedron.delegate(pt);

  return true;
}

bool CGALMesher::closePolyhedron(Polyhedron& polyhedron) {
  BOOST_LOG_TRIVIAL(trace) << "Closing polyhedron ";

  polyhedron.normalize_border();

  BOOST_LOG_TRIVIAL(info) << "There are "
                          << polyhedron.size_of_border_halfedges()
                          << " border halfedges (of total "
                          << polyhedron.size_of_facets() << " facets and "
                          << polyhedron.size_of_vertices() << " vertices)";

  std::vector<CGAL::Polyhedron_3<K>::Halfedge_iterator> borders;
  borders.reserve(polyhedron.size_of_border_halfedges());

  for (auto it = polyhedron.border_halfedges_begin(),
            it_end = polyhedron.halfedges_end();
       it != it_end; ++it)
    borders.push_back(it);

  for (auto it : borders) {
    if (!it->is_border()) continue;

    BOOST_LOG_TRIVIAL(info) << "Filling a hole";

    std::vector<CGAL::Polyhedron_3<K>::Face_handle> faces;
    std::vector<CGAL::Polyhedron_3<K>::Vertex_handle> points;

    CGAL::Polygon_mesh_processing::triangulate_and_refine_hole(
        polyhedron, it,
        CGAL::parameters::use_delaunay_triangulation(true)
            .face_output_iterator(std::back_inserter(faces))
            .vertex_output_iterator(std::back_inserter(points)));

    BOOST_LOG_TRIVIAL(info) << "Added " << faces.size() << " faces and "
                            << points.size() << " points";
    polyhedron.normalize_border();
  }

  BOOST_LOG_TRIVIAL(info) << "Now there are "
                          << polyhedron.size_of_border_halfedges()
                          << " border halfedges (of total "
                          << polyhedron.size_of_facets() << " facets and "
                          << polyhedron.size_of_vertices() << " vertices)";

  unsigned Nnontri = 0;
  for (auto it = polyhedron.facets_begin(); it != polyhedron.facets_end(); ++it)
    if (!it->is_triangle()) ++Nnontri;

  BOOST_LOG_TRIVIAL(info) << "There are " << Nnontri
                          << " non-triangulated facets";

  if (!polyhedron.is_pure_triangle())
    BOOST_LOG_TRIVIAL(warning) << "Oops - polyhedron is not triangulated!";

  BOOST_LOG_TRIVIAL(trace) << "Closing finishes";
  return true;
}

bool CGALMesher::mesh(MeshProperty property) {
  BOOST_LOG_TRIVIAL(trace) << "Start the pre-mesh process";

  polySurfaces.clear();

  for (unsigned i = 0; i < vtkPolyDatas.size(); i++) {
    BOOST_LOG_TRIVIAL(info) << "Preprocess for region " << regionNames[i];

    Polyhedron p = CGAL::Polyhedron_3<K>();

    // Check if user requires region offset
    std::array<float, 3> offset;
    auto it = property.perRegionOffset.find(regionNames[i]);
    if (it != property.perRegionOffset.end()) offset = it->second;

    if (property.smoothFactor != 0) {
      smoothSurface(vtkPolyDatas[i], property.smoothFactor);
    }

    if (!buildPolyhedron(p, vtkPolyDatas[i], offset)) {
      BOOST_LOG_TRIVIAL(error) << "Build polyhedron failed ";
      return false;
    }

    if (!closePolyhedron(p)) {
      BOOST_LOG_TRIVIAL(error) << "Close polyhedron failed ";
      return false;
    }

    polySurfaces.push_back(p);
  }

  // CGAL::Bbox_3 bb =
  //     bbox_3(polySurfaces.begin(), polySurfaces.end(), Polyhedron_bbox());

  // Instantiate polyhedral mesh domain from polyhedron
  // surfacePolyhedral_mesh_domain
  std::vector<Polyhedral_mesh_domain*> fv;

  assert(property.cellSize.size() > 0);

  for (const auto p : polySurfaces | boost::adaptors::indexed()) {
    // TYS: get the cell size for this surface, default to first size if not
    // defined for this index.
    float cellSize = (p.index() < property.cellSize.size())
                         ? property.cellSize[p.index()]
                         : property.cellSize[0];
    Polyhedral_mesh_domain* p_domain =
        new Polyhedral_mesh_domain(p.value(), cellSize);

    fv.push_back(p_domain);
  }

  CGAL::Bbox_3 bb = CGAL::bbox_3(fv.begin(), fv.end(), Polydomain_bbox());

  // Add a bounding box if user required
  if (property.bbox_margin !=
      std::array<double, 6>{{0.0, 0.0, 0.0, 0.0, 0.0, 0.0}}) {
    addBoundingBox(property);
    bb = CGAL::Bbox_3(bb.xmin() - property.bbox_margin[0],
                      bb.ymin() - property.bbox_margin[1],
                      bb.zmin() - property.bbox_margin[2],
                      bb.xmax() + property.bbox_margin[3],
                      bb.ymax() + property.bbox_margin[4],
                      bb.zmax() + property.bbox_margin[5]);
  }

  BOOST_LOG_TRIVIAL(info) << "X Range : " << bb.xmin() << " to " << bb.xmax();
  BOOST_LOG_TRIVIAL(info) << "Y Range : " << bb.ymin() << " to " << bb.ymax();
  BOOST_LOG_TRIVIAL(info) << "Z Range : " << bb.zmin() << " to " << bb.zmax();

  Polyhedral_vector_to_labeled_function_wrapper<Polyhedral_mesh_domain, K>
      labelfunc(fv);

  Mesh_domain domain(labelfunc, bb, fv);
  //            1e-3);			// <= this param gives max length to
  //            subdivide in the event of crossing a domain as a fraction of bb
  //            diagonal

  // print error message if no cell size, facet size or facet distance defined
  if (property.cellSize.empty() || property.facetSize.empty() ||
      property.facetDis.empty()) {
    std::stringstream ss;
    ss << "Not all meshing parameters are defined";
    BOOST_LOG_TRIVIAL(error) << ss.str();
    return false;
  }

  // TYS: subdomain cell sizes
  Sizing_field subdomain_cell_sizes(
      property.cellSize[0]);  // global size defaults to first entry (guarenteed
                              // at least one size)

  Sizing_field subdomain_facet_sizes(property.facetSize[0]);
  Sizing_field subdomain_facet_distances(property.facetDis[0]);

  // this should be true. make sure.
  assert(property.cellSize.size() >= 1 &&
         property.cellSize.size() <= polySurfaces.size());

  // get subdomain sizes (defaults to global size)
  for (unsigned s = 0; s < polySurfaces.size(); s++) {
    // float cellSize = s < property.cellSize.size() ? property.cellSize[s]
    //                                               : property.cellSize[0];
    subdomain_cell_sizes.set_size(property.cellSize[s], 3,
                                  domain.index_from_subdomain_index(s));
    subdomain_facet_sizes.set_size(property.facetSize[s], 3,
                                   domain.index_from_subdomain_index(s));
    subdomain_facet_distances.set_size(property.facetDis[s], 3,
                                       domain.index_from_subdomain_index(s));
  }

  Mesh_criteria criteria(
      CGAL::parameters::facet_angle(property.facetAngle)
          .facet_size(subdomain_facet_sizes)
          .facet_distance(subdomain_facet_distances)
          .cell_radius_edge_ratio(property.cellRadiusEdgeRatio)
          .cell_size(subdomain_cell_sizes));

  BOOST_LOG_TRIVIAL(trace) << "Start meshing... It will take a while";

  // Mesh generation
  Mesh3D c3t3;
  try {
    c3t3 = CGAL::make_mesh_3<Mesh3D>(domain, criteria);
  } catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "Received exception : " << e.what();
    BOOST_LOG_TRIVIAL(error) << "Mesh failed";
    return false;
  }

  triMesh = c3t3.triangulation();

  for (unsigned i = 0; i < fv.size(); i++) delete fv[i];

  meshProperty = property;

  return true;
}

bool CGALMesher::writeToVtkUnstructuredGrid(std::string outputPath,
                                            MeshProperty property) {
  BOOST_LOG_TRIVIAL(trace) << "Writing vtk unstructuredgrid to " << outputPath;

  vtkIntArray* regions = vtkIntArray::New();
  regions->SetNumberOfTuples(triMesh.number_of_finite_cells());

  ////// Range of cells & vertices
  boost::iterator_range<typename Mesh3D::Triangulation::Finite_cells_iterator>
      cells = boost::iterator_range<
          typename Mesh3D::Triangulation::Finite_cells_iterator>(
          triMesh.finite_cells_begin(), triMesh.finite_cells_end());

  for (auto c : cells | boost::adaptors::indexed())
    regions->SetValue(c.index(), c.value().subdomain_index());

  vtkUnstructuredGrid* meshUG = CGALMeshToVTKUnstructuredGrid(triMesh);

  meshUG->GetCellData()->SetScalars(regions);

  vtkUnstructuredGridWriter* UGW = vtkUnstructuredGridWriter::New();
  outputFile = outputPath + ".mesh.vtk";
  UGW->SetFileName(outputFile.c_str());
  if (property.outFileType == BINARY_FILE) {
    UGW->SetFileTypeToBinary();
  } else {
    UGW->SetFileTypeToASCII();
  }
  UGW->SetInputData(meshUG);
  UGW->Update();

  UGW->Delete();
  regions->Delete();

  return true;
}

bool CGALMesher::writeToVtkPolyData(std::string outputPath) {
  // Writing all surfaces out for debug
  vtkPolyDataWriter* W = vtkPolyDataWriter::New();
  for (unsigned i = 0; i < regionNames.size(); i++) {
    std::string filename =
        dir(outputPath) + "/" + basename(regionNames[i]) + ".surf.vtk";
    BOOST_LOG_TRIVIAL(info) << "Writing vtk surface to " << filename;

    vtkPolyData* surfPD =
        CGALPolyhedronSurfaceToPolyData<Polyhedron>(polySurfaces[i]);

    W->SetFileName(filename.c_str());
    W->SetInputData(surfPD);
    W->Update();

    surfPD->Delete();
  }

  W->Delete();

  return true;
}

void CGALMesher::qualityAccess() {
  std::map<int, std::string> regionOrder;
  for (unsigned i = 0; i < regionNames.size(); i++)
    regionOrder.insert(std::make_pair(i, regionNames[i]));

  meshQualityAssess(outputFile, regionOrder);
}
