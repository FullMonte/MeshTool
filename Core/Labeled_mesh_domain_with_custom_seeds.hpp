#ifndef LABELED_MESH_DOMAIN_WITH_CUSTOM_SEEDS_HPP
#define LABELED_MESH_DOMAIN_WITH_CUSTOM_SEEDS_HPP

#include <vector>
// using namespace std;

#include <CGAL/Labeled_mesh_domain_3.h>
#include <CGAL/grid_simplify_point_set.h>

/**
 * Class of labeled mesh domain for multiple polyhedron domains.
 * It is initiliazed with seeds spreading out all the surfaces.
 */

template <class FunctionVectorWrapper, class BGT>
struct Labeled_mesh_domain_with_custom_seeds
    : public CGAL::Labeled_mesh_domain_3<BGT> {
  typedef typename CGAL::Labeled_mesh_domain_3<BGT>::Point_3 Point_3;
  typedef typename CGAL::Labeled_mesh_domain_3<BGT>::Index Index;
  typedef typename CGAL::Labeled_mesh_domain_3<BGT>::Bbox_3 Bbox_3;
  typedef typename CGAL::Labeled_mesh_domain_3<BGT>::FT FT;

  typedef typename FunctionVectorWrapper::Function Function;
  typedef typename FunctionVectorWrapper::Function_vector Function_vector;

  // Function vector
  Function_vector function_vector_;

  Labeled_mesh_domain_with_custom_seeds(const FunctionVectorWrapper& f,
                                        const Bbox_3& bbox, Function_vector& fv,
                                        const FT& error_bound = FT(1e-6))
      : CGAL::Labeled_mesh_domain_3<BGT>(
            f, bbox, CGAL::parameters::relative_error_bound(error_bound)),
        function_vector_(fv) {}

  /**
   * Constructs  a set of \ccc{n} points on the surface, and output them to
   *  the output iterator \ccc{pts} whose value type is required to be
   *  \ccc{std::pair<Points_3, Index>}.
   */
  struct Construct_initial_points {
    Construct_initial_points(
        const Labeled_mesh_domain_with_custom_seeds& domain)
        : r_domain_(domain) {}

    template <class OutputIterator>
    OutputIterator operator()(OutputIterator pts, const int n = 12) const {
      size_t subdomain_num = r_domain_.function_vector_.size();

      for (size_t i = 0; i < subdomain_num; i++) {
        Function* subdomain = r_domain_.function_vector_[i];

        std::vector<std::pair<Point_3, Index>> seeds_vector;

        typename Function::Construct_initial_points initial_points =
            subdomain->construct_initial_points_object();
        initial_points(std::back_inserter(seeds_vector), n);

        // Create nb_point points
        size_t n = seeds_vector.size();
#ifdef CGAL_MESH_3_VERBOSE
        std::cout << "Constructing " << n << " initial points in domain "
                  << i + 1 << std::endl;
#endif
        for (std::size_t i = 0, end = seeds_vector.size(); i < end; ++i) {
          *pts++ = std::make_pair(seeds_vector[i].first, i + 1);
        }
      }

      return pts;
    }

   private:
    const Labeled_mesh_domain_with_custom_seeds& r_domain_;
  };

  /// Returns Construct_initial_points object
  Construct_initial_points construct_initial_points_object() const {
    return Construct_initial_points(*this);
  }
};

#endif  // LABELED_MESH_DOMAIN_WITH_CUSTOM_SEEDS_HPP
