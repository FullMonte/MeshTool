#ifndef VTKPOLYTOCGAL_HPP
#define VTKPOLYTOCGAL_HPP
#include <vector>
#include <string>
#include <array>
#include <vtkPolyData.h>

std::pair<std::vector<std::array<float,3>>,std::vector<std::array<unsigned,3>>> VTKPolyToPT(std::string inputFile,int smoothFactor=0);

std::pair<std::vector<std::array<float,3>>,std::vector<std::array<unsigned,3>>> VTKPolyToPT(vtkPolyData* poly);

#endif // VTKPOLYTOCGAL_HPP
