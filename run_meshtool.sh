
xhost +local:docker

export DISPLAY=:2


docker run --rm -ti \
            --net=host \
            -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 \
            -v $PWD:/builds/FullMonte/MeshTool \
            -e DISPLAY=$DISPLAY\
            meshtool-run:ubuntu
        

